package com.icm.testjboss7.session;

import com.icm.testjboss7.entity.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityHome;

@Name("usersHome")
public class UsersHome extends EntityHome<Users> {

	public void setUsersId(Long id) {
		setId(id);
	}

	public Long getUsersId() {
		return (Long) getId();
	}

	@Override
	protected Users createInstance() {
		Users users = new Users();
		return users;
	}

	public void load() {
		if (isIdDefined()) {
			wire();
		}
	}

	public void wire() {
		getInstance();
	}

	public boolean isWired() {
		return true;
	}

	public Users getDefinedInstance() {
		return isIdDefined() ? getInstance() : null;
	}

}
