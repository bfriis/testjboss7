package com.icm.testjboss7.session;

import com.icm.testjboss7.entity.*;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.framework.EntityQuery;
import java.util.Arrays;

@Name("usersList")
public class UsersList extends EntityQuery<Users> {

	private static final String EJBQL = "select users from Users users";

	private static final String[] RESTRICTIONS = {};

	private Users users = new Users();

	public UsersList() {
		setEjbql(EJBQL);
		setRestrictionExpressionStrings(Arrays.asList(RESTRICTIONS));
		setMaxResults(25);
	}

	public Users getUsers() {
		return users;
	}
}
